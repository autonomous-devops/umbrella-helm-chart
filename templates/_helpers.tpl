{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "common.name" -}}
{{- if .Values.ingress.canary.enabled }}
{{- printf "%s-canary" .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- default .Chart.Name .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "common.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- if .Values.ingress.canary.enabled }}
{{- printf "%s-canary" .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end -}}
{{- end -}}

{{- define "common.canaryName" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- if or .Values.ingress.canary.enabled .Values.istio.canary.enabled }}
{{- if eq .Values.appVersion "baseline" -}}
{{- printf "%s-baseline" .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- else if eq .Values.appVersion "canary" }}
{{- printf "%s-canary" .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- else }}
{{- .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "common.workloadselector" -}}
{{- if .Values.workloadlabel }}
{{- .Values.workloadlabel | trunc 63 -}}
{{- else }}
{{- if .Values.ingress.canary.enabled }}
{{- printf "deployment-%s-%s-canary" .Values.namespace .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- printf "deployment-%s-%s" .Values.namespace .Values.projectName | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end -}}
{{- end -}}

{{/*
Distribute Canary proportion
*/}}
{{- define "common.canaryWeight" -}}
{{- if eq .Values.appVersion "canary" }}
{{- if .Values.istio.canary.canaryWeight }}
{{- .Values.istio.canary.canaryWeight -}}
{{- else }}
{{- 1 -}}
{{- end }}
{{- else }}
{{- 0 -}}
{{- end -}}
{{- end -}}
{{- define "common.baselineWeight" -}}
{{- if .Values.hasBaseline }}
{{- if .Values.istio.canary.canaryWeight }}
{{- .Values.istio.canary.canaryWeight -}}
{{- else }}
{{- 1 -}}
{{- end }}
{{- else }}
{{- 0 -}}
{{- end -}}
{{- end -}}
{{- define "common.mainWeight" -}}
{{- $canaryWeight := include "common.canaryWeight" . -}}
{{- $baselineWeight := include "common.baselineWeight" . -}}
{{- if eq .Values.appVersion "main" }}
{{- 100  -}}
{{- else }}
{{- add 100 (mul -1 $canaryWeight) (mul -1 $baselineWeight) -}}
{{- end -}}
{{- end -}}
